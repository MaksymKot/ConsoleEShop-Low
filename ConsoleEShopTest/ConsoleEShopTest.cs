using System;
using Xunit;

namespace ConsoleEShop
{

    public class ConsoleEShopTest
    {
        [Fact]
        public void ThrowExceptionIfAnyUserCanNotViewProductsInNotExistingDataBase()
        {
            //arrange
            var guest = new Guest();
            //assert
            Assert.Throws<NullReferenceException>(() => guest.View(null));
        }

        [Fact]
        public void ThrowExceptionDuringCheckoutWhileDataBaseIsNull()
        {
            //arrange
            var user = get_user();
            //assert
            Assert.Throws<ArgumentNullException>(() => user.�heckOut(null));
        }

        [Fact]
        public void ThrowExceptionDuringUserRegistrationWhenLoginIsAlreadyTaken()
        {
            //arrange
            var guest = new Guest();
            var as_admin = false;
            var user_db = new RegisteredUsersDataBase(get_user());
            var db = new DataBase(null, user_db, null);
            var taken_login = get_user().Login;
            var password = 123;
            //assert
            Assert.Throws<Exception>(() => guest.Register(db, taken_login, password, as_admin));
        }

        [Fact]
        public void ThrowExceptionDuringAdminRegistrationWhenLoginIsAlreadyTaken()
        {
            //arrange
            var guest = new Guest();
            var as_admin = true;
            var admin_db = new AdminDataBase(get_admin());
            var db = new DataBase(null, null, admin_db);
            var taken_login = get_admin().Login;
            var password = 123;
            //assert
            Assert.Throws<Exception>(() => guest.Register(db, taken_login, password, as_admin));
        }

        [Fact]
        public void UsersDataBaseThrowsExceptionifCanNotFindUserInDataBase()
        {
            //arrange
            var user_db = new RegisteredUsersDataBase();
            var incorrect_login = "azerty";
            // act
            Assert.Throws<Exception>(() => user_db.FindUser(incorrect_login));
        }

        [Fact]
        public void MenuThrowsExceptionIfDataBaseIsNull()
        {
            Assert.Throws<Exception>(() => new Menu(null).Start());
        }

        [Fact]
        public void ExceptionWhileAddingNewOrderWithNotExistingProduct()
        {
            //arrange
            var db = new DataBase();
            var order_db = new OrderDataBase();
            //assert
            Assert.Throws<ArgumentNullException>(() => order_db.AddOrder(db, null));
        }

        [Fact]
        public void ExceptionWhileAddingNewOrderToNotExistingDataBase()
        {
            //arrange
            var prod = get_prod();
            var order_db = new OrderDataBase();
            //assert
            Assert.Throws<NullReferenceException>(() => order_db.AddOrder(null, prod));
        }

        [Fact]
        public void UserMakeOrderWithNullProductReturnException()
        {
            //arrange
            var user = get_user();
            //assert
            Assert.Throws<ArgumentNullException>(() => user.makeOrder(null));
        }

        [Fact]
        public void AdminMakeOrderWithNullProductReturnException()
        {
            //arrange
            var admin = get_admin();
            //assert
            Assert.Throws<ArgumentNullException>(() => admin.MakeOrder(null));
        }

        [Fact]
        public void UserDataBaseNewUserHasBeenAdded()
        {
            //arrane
            var user1 = get_user();
            var user2_login = "user2";
            var user2_password = 123;
            var user_db = new RegisteredUsersDataBase(user1);
            //act
            user_db.AddUser(user2_login, user2_password);
            var new_admin = user_db.FindUser(user2_login);
            var expected = true;
            var res = new_admin.Login.Equals(user2_login) && new_admin.Password == user2_password;
            //assert
            Assert.Equal(expected, res);
        }

        [Fact]
        public void AdminDataBaseNewAdminHasBeenAdded()
        {
            //arrane
            var admin1 = get_admin();
            var admin2_login = "admin2";
            var admin2_password = 123;
            var admin_db = new AdminDataBase(admin1);
            //act
            admin_db.AddUser(admin2_login, admin2_password);
            var new_admin = admin_db.FindUser(admin2_login);
            var expected = true;
            var res = new_admin.Login.Equals(admin2_login) && new_admin.Password == admin2_password;
            //assert
            Assert.Equal(expected, res);
        }

        [Fact]
        public void GuestLogInAdminFailed()
        {
            //arrange
            var admin = get_admin();
            var guest = new Guest();
            var admin_db = new AdminDataBase(admin);
            var db = new DataBase(null, null, admin_db);
            var as_admin = true;
            //act
            var incorrect_password = 0000;
            var res = guest.LogIn(db, admin.Login, incorrect_password, as_admin);
            var expected = false;
            //assert
            Assert.Equal(expected, res);
        }

        [Fact]
        public void GuestLogInAdminSuccessful()
        {
            //arrange
            var admin = get_admin();
            var guest = new Guest();
            var admin_db = new AdminDataBase(admin);
            var db = new DataBase(null, null, admin_db);
            var as_admin = true;
            //act
            var res = guest.LogIn(db, admin.Login, admin.Password, as_admin);
            var expected = true;
            //assert
            Assert.Equal(expected, res);
        }

        [Fact]
        public void GuestLogInUserFailer()
        {
            //arrange
            var user = get_user();
            var guest = new Guest();
            var user_db = new RegisteredUsersDataBase(user);
            var db = new DataBase(null, user_db, null);
            var as_admin = false;
            //act
            var incorrect_password = 9878;
            var res = guest.LogIn(db, user.Login, incorrect_password, as_admin);
            var expected = false;
            //assert
            Assert.Equal(expected, res);

        }

        [Fact]
        public void GuestLogInUserSuccessful()
        {
            //arrange
            var user = get_user();
            var guest = new Guest();
            var user_db = new RegisteredUsersDataBase(user);
            var db = new DataBase(null, user_db, null);
            var as_admin = false;
            //act
            var res = guest.LogIn(db, user.Login, user.Password, as_admin);
            var expected = true;
            //assert
            Assert.Equal(expected, res);
        }

        [Fact]
        public void UserMadeOrder()
        {
            // arrange
            var user = get_user();
            var reg_user_db = new RegisteredUsersDataBase(user);
            var prod_db = new ProductDataBase(get_prod());
            var db = new DataBase(prod_db, reg_user_db, null);
            // act
            user.makeOrder(get_prod());
            user.�heckOut(db);
            var res = user.OrderHistory.db.Find(x => x.Product.Equals(get_prod()));
            var expected = "New";
            //assert
            Assert.Equal(res.Status, expected);
        }

        private RegisteredUser get_user()
        {
            return new RegisteredUser()
            {
                Login = "qwerty",
                Name = "John",
                Password = 123,
            };
        }

        private Admin get_admin()
        {
            return new Admin()
            {
                Login = "Qwerty",
                Password = 123,
                Name = "Bob",
            };
        }

        private Product get_prod()
        {
            return new Product()
            {
                Name = "PocketBook",
                Price = 120,
                Category = "E-readers",
            };
        }
    }
}
