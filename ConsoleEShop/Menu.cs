﻿using System;

namespace ConsoleEShop
{
    public class Menu
    {
        private DataBase db;
        public Menu()
        {
            throw new Exception("No database");
        }
        public Menu(DataBase database)
        {
            if (database is null)
                throw new Exception("No database");
            db = database;
        }

        public void Start()
        {
            var guest = new Guest();
            var exit = false;
            while (!exit)
            {
                try
                {
                    var log_in = false;
                    SeeGuestMenu();
                    var choise = Console.ReadLine();
                    switch (choise)
                    {
                        case "1":
                            guest.View(db);
                            break;

                        case "2":

                            Console.WriteLine("\nEnter the name\n");
                            var name = Console.ReadLine();
                            Console.WriteLine("\nEnter the category\n");
                            var category = Console.ReadLine();
                            var Prod = guest.SearchItem(db, name, category);
                            Prod.SeeProdInfo();
                            break;

                        case "3":
                            var admin = false;
                            Console.WriteLine("\nRegister as admin?\n");
                            var answr = Console.ReadLine();
                            bool as_admin;
                            if (answr.ToUpper().Equals("Y") || answr.Equals("Y"))
                                admin = true;
                            if (answr.ToUpper().Equals("N") || answr.Equals("N"))
                                admin = false;

                            Console.WriteLine("\nEnter the login\n");
                            var login = Console.ReadLine();
                            Console.WriteLine("\nEnter the password\n");
                            var password = int.Parse(Console.ReadLine());
                            guest.Register(db, login, password, admin);
                            break;

                        case "5":
                            exit = true;
                            break;

                        case "4":
                            Console.WriteLine("\nDo you want to log in as admin? Y/N\n");
                            var answer = Console.ReadLine();
                            as_admin = false;
                            if (answer.ToUpper().Equals("Y") || answer.Equals("Y"))
                                as_admin = true;
                            if (answer.ToUpper().Equals("N") || answer.Equals("N"))
                                as_admin = false;


                            if (as_admin)
                            {
                                Console.WriteLine("\nEnter the login\n");
                                login = Console.ReadLine();
                                Console.WriteLine("\nEnter the password\n");
                                password = int.Parse(Console.ReadLine());

                                if (guest.LogIn(db, login, password, as_admin))
                                {
                                    var admn = db.Admin.FindUser(login);
                                    bool admn_log_out = false;

                                    while (!admn_log_out)
                                    {
                                        try
                                        {
                                            Console.WriteLine($"\nLogged as :{admn.Login}");
                                            SeeAdminMenu();
                                            choise = Console.ReadLine();
                                            switch (choise)
                                            {
                                                case "1":
                                                    admn.View(db);
                                                    break;

                                                case "2":
                                                    Console.WriteLine("\nEnter the name\n");
                                                    name = Console.ReadLine();
                                                    Console.WriteLine("\nEnter the category\n");
                                                    category = Console.ReadLine();
                                                    var prod = admn.SearchItem(db, name, category);
                                                    prod.SeeProdInfo();
                                                    break;

                                                case "3":
                                                    Console.WriteLine("\nEnter the name of product which you are looking for\n");
                                                    string prod_name = Console.ReadLine();
                                                    Console.WriteLine("\nEnter the category of product which you are looking for\n");
                                                    string prod_category = Console.ReadLine();
                                                    admn.MakeOrder(db.Product.SearchProduct(
                                                        db, prod_name, prod_category));
                                                    break;

                                                case "4":
                                                    admn.СheckOut(db);
                                                    break;

                                                case "5":
                                                    Console.WriteLine("\nEnter user's login\n");
                                                    var u_logn = Console.ReadLine();
                                                    admn.SeeUserInfo(db, u_logn);
                                                    break;

                                                case "6":
                                                    Console.WriteLine("\nEnter login of user\n");
                                                    var u_login = Console.ReadLine();
                                                    Console.WriteLine("\nEnter new information\n");
                                                    var new_u_info = Console.ReadLine();
                                                    admn.ChangeUserInfo(db, u_login, new_u_info);
                                                    break;

                                                case "7":
                                                    Console.WriteLine("\nEnter name of new product\n");
                                                    var new_prod_name = Console.ReadLine();
                                                    Console.WriteLine("\nEnter price of new product\n");
                                                    var new_prod_price = int.Parse(Console.ReadLine());
                                                    Console.WriteLine("\nEnter category of new  product\n");
                                                    var new_prod_category = Console.ReadLine();
                                                    Console.WriteLine("\nEnter info to new product\n");
                                                    var new_prod_info = Console.ReadLine();
                                                    admn.AddNewProduct(db, new_prod_price, new_prod_name, new_prod_info, new_prod_category);
                                                    break;

                                                case "8":
                                                    Console.WriteLine("\nEnter name of product\n");
                                                    var name_prod = Console.ReadLine();
                                                    Console.WriteLine("\nEnter new info of product\n");
                                                    var info_prod = Console.ReadLine();
                                                    Console.WriteLine("\nEnter category of  product\n");
                                                    var category_prod = Console.ReadLine();
                                                    admn.ChangeProductInfo(db, info_prod, name_prod, category_prod);
                                                    break;

                                                case "9":
                                                    Console.WriteLine("\nFind order among users?\n");
                                                    var res = Console.ReadLine();
                                                    bool user_order = false;

                                                    if (res.Equals("Y") || res.ToUpper().Equals("Y"))
                                                        user_order = true;

                                                    Console.WriteLine("\nEnter id of order\n");
                                                    var order_id = int.Parse(Console.ReadLine());
                                                    Console.WriteLine("\nEnter new status of order\n");
                                                    var stat = Console.ReadLine();

                                                    admn.SetOrdersStatus(db, order_id, stat, user_order);
                                                    break;

                                                case "10":
                                                    admn.SeeShoppingCard();
                                                    break;
                                                case "11":
                                                    admn_log_out = true;
                                                    break;

                                                default:
                                                    break;
                                            }
                                        }
                                        catch (Exception e) { Console.WriteLine(e.Message); }
                                    }




                                }

                            }
                            else
                            {
                                Console.WriteLine("\nEnter the login\n");
                                login = Console.ReadLine();
                                Console.WriteLine("\nEnter the password\n");
                                password = int.Parse(Console.ReadLine());

                                if (guest.LogIn(db, login, password, as_admin))
                                {

                                    var user = db.User.FindUser(login);
                                    bool user_log_out = false;
                                    while (!user_log_out)
                                    {
                                        try
                                        {
                                            Console.WriteLine($"\nLogged as :{user.Login}");
                                            SeeUserMenu();
                                            choise = Console.ReadLine();
                                            switch (choise)
                                            {
                                                case "1":
                                                    user.View(db);
                                                    break;

                                                case "2":
                                                    Console.WriteLine("\nEnter the name\n");
                                                    name = Console.ReadLine();
                                                    Console.WriteLine("\nEnter the category\n");
                                                    category = Console.ReadLine();
                                                    var prod = user.SearchItem(db, name, category);
                                                    prod.SeeProdInfo();
                                                    break;

                                                case "3":
                                                    Console.WriteLine("\nEnter the name of product which you are looking for\n");
                                                    string prod_name = Console.ReadLine();
                                                    Console.WriteLine("\nEnter the category of product which you are looking for\n");
                                                    string prod_category = Console.ReadLine();
                                                    user.makeOrder(db.Product.SearchProduct(
                                                        db, prod_name, prod_category));
                                                    break;

                                                case "4":
                                                    user.СheckOut(db);
                                                    break;

                                                case "5":
                                                    Console.WriteLine("\nEnter id of your order\n");
                                                    var id = int.Parse(Console.ReadLine());
                                                    user.CancelOrder(id);
                                                    break;

                                                case "6":
                                                    user.seeOrderHistory();
                                                    break;

                                                case "7":
                                                    Console.WriteLine("\nEnter id of  order\n");
                                                    var id_order = int.Parse(Console.ReadLine());
                                                    var ordr = user.OrderHistory.db.Find(x => x.Id == id_order);
                                                    if (!(ordr is null))
                                                        Console.WriteLine($"\nOrder's status : {ordr.Status}\n");
                                                    else
                                                        Console.WriteLine($"\nOrder with this id doesnt exist\n");

                                                    break;

                                                case "8":
                                                    Console.WriteLine("\nEnter id of your searching product\n");
                                                    var order_id = int.Parse(Console.ReadLine());
                                                    user.setOrdersStatusToReceived(order_id);
                                                    break;

                                                case "9":
                                                    Console.WriteLine("\nEnter new name\n");
                                                    string new_name = Console.ReadLine();
                                                    Console.WriteLine("\nEnter new login\n");
                                                    string new_login = Console.ReadLine();
                                                    Console.WriteLine("\nEnter new info\n");
                                                    string new_info = Console.ReadLine();
                                                    Console.WriteLine("\nEnter new password\n");
                                                    int new_password = int.Parse(Console.ReadLine());

                                                    user.changeinfo(new_name, new_password, new_login, new_info);
                                                    break;

                                                case "10":
                                                    user.SeeShoppingCard();
                                                    break;

                                                case "11":
                                                    user_log_out = true;
                                                    break;

                                                default:
                                                    throw new Exception("\nThere is not another option\n");
                                                    break;
                                            }
                                        }
                                        catch (Exception e) { Console.WriteLine(e.Message); }
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("\nCannot log in this account\n");
                                    break;
                                }
                            }
                            break;

                        default:
                            throw new Exception("\nThere is not another option\n");
                            break;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine($"\n{e.Message}\n");
                }
            }
            Console.WriteLine("\nShutting down\n");
        }

        void SeeAdminMenu()
        {
            Console.WriteLine("\n\t Admins's menu:");
            Console.WriteLine("1) - View all products");
            Console.WriteLine("2) - Find product by name");
            Console.WriteLine("3) - Make order");
            Console.WriteLine("4) - Checkout  order");
            Console.WriteLine("5) - See user's info");
            Console.WriteLine("6) - change user's info");
            Console.WriteLine("7) - Add new product");
            Console.WriteLine("8) - Change product's info");
            Console.WriteLine("9) - Change order's status");
            Console.WriteLine("10 - See your shopping card");
            Console.WriteLine("11 - log out");
            Console.WriteLine("\n Please make a choice\n");
        }

        void SeeGuestMenu()
        {
            Console.WriteLine("\t Guest's menu:");
            Console.WriteLine("1) - View all products");
            Console.WriteLine("2) - Find product by name");
            Console.WriteLine("3) - Register");
            Console.WriteLine("4) - Log in");
            Console.WriteLine("5) - Exit");
            Console.WriteLine("\n Please make a choice\n");
        }

        void SeeUserMenu()
        {
            Console.WriteLine("\n\t User's menu:");
            Console.WriteLine("1) - View all products");
            Console.WriteLine("2) - Find product by name");
            Console.WriteLine("3) - Make order");
            Console.WriteLine("4) - Checkout order");
            Console.WriteLine("5) - Cancel order");
            Console.WriteLine("6) - See order history");
            Console.WriteLine("7) - See  order's status");
            Console.WriteLine("8) - Change order's status to \"Received\"");
            Console.WriteLine("9) - Change your personal info");
            Console.WriteLine("10 - View your shopping card");
            Console.WriteLine("11 - log out");
            Console.WriteLine("\n Please make a choice\n");
        }

    }
}
