﻿using System;

namespace ConsoleEShop
{
    public class Admin : User
    {
        public int Password { get; set; }
        public string Login { get; set; }
        public string Name { get; set; }
        public string Info { get; set; } = "No info";
        private ShoppingCard Card { get; set; } = new ShoppingCard();
        public OrderDataBase OrderHistory { get; set; } = new OrderDataBase();


        public void MakeOrder(Product product)
        {
            if (product is null)
                throw new ArgumentNullException("No products");
            Card.ShopCard.Add(product);
            Console.WriteLine($"\n{product.Name} has been added to shopping card\n");
        }

        public void SetOrdersStatus(DataBase db, int id, string status, bool user_order)
        {
            if (user_order)
            {
                var a = db.User.db.Find(x => x.OrderHistory.db.Exists(y => y.Id == id));
                var b = a.OrderHistory.db.Find(x => x.Id == id);
                if (!(b is null))
                {
                    switch (status)
                    {
                        case Status.COMPLETED:
                            b.Status = status;
                            break;
                        case Status.DONE:
                            b.Status = status;
                            break;
                        case Status.RECEIVED_PAYMENT:
                            b.Status = status;
                            break;
                        case Status.SHIPPED:
                            b.Status = status;
                            break;
                        case Status.CANCELLED_BY_ADMIN:
                            b.Status = status;
                            break;
                        case Status.RECEIVED:
                            b.Status = status;
                            break;
                        default:
                            throw new Exception("Invalid status");
                    }
                }
                else
                {
                    throw new Exception("There is not order with this id");
                }

            }
            else
            {
                var a = db.Admin.db.Find(x => x.OrderHistory.db.Exists(y => y.Id == id));
                var b = a.OrderHistory.db.Find(x => x.Id == id);
                if (!(b is null))
                {
                    switch (status)
                    {
                        case Status.COMPLETED:
                            b.Status = status;
                            break;
                        case Status.DONE:
                            b.Status = status;
                            break;
                        case Status.RECEIVED_PAYMENT:
                            b.Status = status;
                            break;
                        case Status.SHIPPED:
                            b.Status = status;
                            break;
                        case Status.CANCELLED_BY_ADMIN:
                            b.Status = status;
                            break;
                        case Status.RECEIVED:
                            b.Status = status;
                            break;
                        default:
                            throw new Exception("Invalid status");
                    }
                }
                else
                {
                    throw new Exception("There is not order with this id");
                }
            }
        }

        public void View(DataBase db)
        {
            if (db is null)
                throw new NullReferenceException("no reference to database");
            db.Product.GetInfo();
        }

        public void SeeUserInfo(DataBase db, string login)
        {
            if (db.User.UserContains(login))
            {
                var user = db.User.FindUser(login);
                Console.WriteLine("User's info:");
                Console.WriteLine($"Name : {user.Name}");
                Console.WriteLine($"Login : {user.Login}");
                Console.WriteLine($"Password : {user.Password}");
                Console.WriteLine($"Info : {user.Info}");
            }
            else
                throw new Exception("There is not user with this login");
        }

        public void СheckOut(DataBase db)
        {
            if (db is null)
                throw new ArgumentNullException("Database is not connected");

            if (Card.ShopCard.Count == 0)
            {
                Console.WriteLine("\nYour shopping card is empty\n");
            }
            else
            {
                foreach (var item in Card.ShopCard)
                {
                    OrderHistory.AddOrder(db, item);
                }
                Card.ShopCard.Clear();
            }

        }

        public void SeeShoppingCard()
        {

            if (Card.ShopCard.Count == 0)
            {
                Console.WriteLine("\nYour shopping card is emply\n");
            }
            else
            {
                Console.WriteLine("\nYour shopping card:");
                foreach (var item in Card.ShopCard)
                {
                    Console.WriteLine($"\nName of product: {item.Name}\n");
                    Console.WriteLine($"\nPrice of product: {item.Price}\n");
                    Console.WriteLine($"\nInfo about product: {item.Info}\n");
                    Console.WriteLine($"\nCategory of product: {item.Category}\n");
                }
                Console.WriteLine();
            }
        }

        public void ChangeUserInfo(DataBase db, string login, string info)
        {
            var account = db.User.FindUser(login);
            account.Info = info;
            Console.WriteLine("\nInfo has been updated by admin");
        }

        public void AddNewProduct(DataBase db, int price, string name
            , string info, string category)
        {
            db.Product.AddNewItem(new Product()
            {
                Price = price,
                Name = name,
                Info = info,
                Category = category,
            });
        }

        public void ChangeProductInfo(DataBase db, string info, string name, string caterogy)
        {
            if (db.Product.ProductContains(db, name, caterogy))
            {
                var prod = db.Product.SearchProduct(db, name, caterogy);
                prod.Info = info;
            }

            else
                throw new Exception("There is not this product");
        }

    }
}
