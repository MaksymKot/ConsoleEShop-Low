﻿using System;
using System.Collections.Generic;

namespace ConsoleEShop
{
    public class RegisteredUsersDataBase
    {
        public List<RegisteredUser> db { get; }

        public RegisteredUsersDataBase()
        {
            db = new List<RegisteredUser>();
        }

        public RegisteredUsersDataBase(params RegisteredUser[] users)
        {
            db = new List<RegisteredUser>();
            foreach (var item in users)
            {
                db.Add(item);
            }
        }

        public RegisteredUsersDataBase(IEnumerable<RegisteredUser> users)
        {
            db = new List<RegisteredUser>(users);
        }

        public bool UserContains(string login)
        {
            foreach (var item in db)
                if (item.Login == login)
                    return true;
            return false;
        }

        public RegisteredUser FindUser(string login)
        {
            if (UserContains(login))
                return db.Find(x => x.Login == login);
            else
                throw new Exception($"There is not account with login: {login}");
        }

        public void AddUser(string login, int password)
        {
            db.Add(new RegisteredUser() { Login = login, Password = password });
        }
    }
}
