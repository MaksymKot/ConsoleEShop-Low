﻿using System.Collections.Generic;

namespace ConsoleEShop
{
    public class AdminDataBase
    {
        public List<Admin> db { get; }

        public AdminDataBase()
        {
            db = new List<Admin>();
        }

        public AdminDataBase(params Admin[] users)
        {
            db = new List<Admin>();
            foreach (var item in users)
            {
                db.Add(item);
            }
        }

        public AdminDataBase(IEnumerable<Admin> users)
        {
            db = new List<Admin>(users);
        }

        public bool UserContains(string login)
        {
            return db.Contains(FindUser(login));
        }

        public Admin FindUser(string login)
        {
            return db.Find(x => x.Login.Equals(login));
        }

        public void AddUser(string login, int password)
        {
            db.Add(new Admin() { Login = new string(login), Password = password });
        }
    }
}
