﻿using System;

namespace ConsoleEShop
{
    public class Product
    {
        public int Price { get; set; }
        public string Name { get; set; }
        public string Info { get; set; } = "No info";
        public string Category { get; set; } = "No info";
        private static int Count { get; set; } = 0;
        public int Id { get; }

        public Product()
        {
            Count++;
            Id = Count;
        }

        public void SeeProdInfo()
        {
            Console.WriteLine("Information about product:");
            Console.WriteLine($"Name : {Name}");
            Console.WriteLine($"Id : {Id}");
            Console.WriteLine($"Category : {Category}");
            Console.WriteLine($"Info : {Info}");
            Console.WriteLine($"Price : {Price}");
            Console.WriteLine("______________________");
        }
        public override bool Equals(object obj)
        {
            var arg = obj as Product;
            return this.Price == arg.Price && this.Name.Equals(arg.Name)
                && this.Info.Equals(arg.Info) && this.Category.Equals(arg.Category);
        }
    }
}
