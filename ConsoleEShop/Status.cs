﻿namespace ConsoleEShop
{
    public static class Status
    {
        public const string SHIPPED = "Shipped";
        public const string DONE = "Done";
        public const string CANCELLED_BY_ADMIN = "Cancelled by admin";
        public const string CANCELLED_BY_USER = "Cancelled by user";
        public const string RECEIVED_PAYMENT = "Received payment";
        public const string RECEIVED = "Received";
        public const string COMPLETED = "Completed";
    }
}
