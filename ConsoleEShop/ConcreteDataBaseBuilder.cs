﻿namespace ConsoleEShop
{
    public class ConcreteDataBaseBuilder : DataBaseBuilder
    {
        private DataBase currentBuilder;

        public override void CreateDataBase()
        {
            currentBuilder = new DataBase();
        }

        public override void CreateAdminDB()
        {
            currentBuilder.Admin = make_admin_db();
        }

        public override void CreateProductDB()
        {
            currentBuilder.Product = make_prod_db();
        }

        public override void CreateUserDB()
        {
            currentBuilder.User = make_reg_users_db();
        }

        public override DataBase GetDataBase() => currentBuilder;

        ProductDataBase make_prod_db()
        {
            return new ProductDataBase(
                new Product { Name = "Apple", Price = 5, Category = "Food" },
                new Product { Name = "Macbook Air", Price = 782, Category = "Notebooks" },
                new Product { Name = "Amazon Kindle 10th Gen", Price = 150, Category = "E-readers" });
        }

        RegisteredUsersDataBase make_reg_users_db()
        {
            return new RegisteredUsersDataBase(
                new RegisteredUser() { Login = "Qworty", Name = "Bob", Password = 2345 },
                new RegisteredUser() { Login = "Qwarty", Name = "Jack", Password = 3211 });
        }

        AdminDataBase make_admin_db()
        {
            return new AdminDataBase(
                new Admin() { Login = "Qwerty", Password = 123 },
                new Admin() { Login = "Azerty", Password = 321 },
                new Admin() { Login = "Qwarty", Password = 234 });
        }

    }
}
