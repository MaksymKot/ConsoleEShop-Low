﻿namespace ConsoleEShop
{
    public class DataBase
    {
        public ProductDataBase Product { get; set; }
        public RegisteredUsersDataBase User { get; set; }
        public AdminDataBase Admin { get; set; }

        public DataBase() { }
        public DataBase(ProductDataBase products, RegisteredUsersDataBase users, AdminDataBase admins)
        {
            Product = products;
            User = users;
            Admin = admins;
        }

    }
}
